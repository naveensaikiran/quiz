(function(){
    var app = angular.module("myApp",[]);   
    app.directive("navbarDirective",function(){
       return{
           restrict : 'E',
           templateUrl : './templates/navbar.html'
       } ;
    });
})();